import "./App.css";
import React from "react";
import { Poll } from "../components/poll/poll";
import { Vote } from "../components/vote/vote";
import { VgChart } from "../components/chart/chart";
import { ObservablePoll } from "../stores/ObservablePoll";
import { observer } from "mobx-react";

type Props = {
  title?: string;
  store: ObservablePoll;
};

//@observer
const Application: React.FC<Props> = observer((props) => {
  const poll = props.store;
  return (
    <div className="container-fluid">
      <div className="row">
        <span className="title">{props.title}</span>
      </div>
      <div className="row content-wrapper">
        <div className="col-4">
          <Poll store={poll} />
        </div>
        <div className="col-4">
          <Vote store={poll} onClick={poll.toVote} />
        </div>
        <div className="col-4">
          <VgChart store={poll} height={400} width={400} padding={20} />
        </div>
      </div>
    </div>
  );
});

export const App = Application;
