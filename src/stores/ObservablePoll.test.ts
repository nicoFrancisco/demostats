import { ObservablePoll } from '../stores/ObservablePoll';

let poll = new ObservablePoll();

beforeEach(() => {
    poll = new ObservablePoll();
})

test('default values for answers', () => {
    expect(poll.answers.size).toBe(3);
    expect(poll.answers.get('3.14')).toStrictEqual({text: '3.14', counter: 5});
    expect(poll.answers.get('3.1416')).toStrictEqual({text: '3.1416', counter: 2});
    expect(poll.answers.get('3.1415964')).toStrictEqual({text: '3.1415964', counter: 1});
})

test('default question text', () => {
    expect(poll.question).toBe('What is the value of Pi?');
})

test('get report', () => {
    expect(poll.report).toBe('3/10 possible answers');
})

test('get voted', () => {
    expect(poll.voted).toBe(8);
})

test('get data', () => {
    expect(poll.data).toStrictEqual([...poll.answers.values()]);
})

test('question length less than 80 characters', () => {
    expect(poll.disabled).toBe(false);
})

test('question length eq 80 characters', () => {
    poll.addQuestion('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temporio');
    expect(poll.question.length).toEqual(80);
    expect(poll.disabled).toBe(true);
})

test('add answer', () => {
    poll.addAnswer('test');
    expect(poll.answers.get('test')).toStrictEqual({text: 'test', counter: 0});
})

test('not allowed insert an answer without question', () => {
    poll.reset();
    poll.addAnswer('test');
    expect(poll.answers.get('test')).toBeUndefined();
})

test('remove answer', () => {
    poll.removeAnswer('3.14');
    expect(poll.answers.get('3.14')).toBeUndefined();
})

test('check min number of answers', () => {
    poll.removeAnswer('3.14');
    poll.removeAnswer('3.1416');
    expect(poll.answers.size).toBe(2);
})

test('to vote an answer', () => {
    poll.addAnswer('test');
    poll.toVote('test');
    expect(poll.answers.get('test')!.counter).toBe(1);
})

test('user can vote many time as possible', () => {
    poll.addAnswer('test');
    poll.toVote('test');
    poll.toVote('test');
    poll.toVote('test');
    expect(poll.answers.get('test')!.counter).toBe(3);
})

test('reset', () => {
    poll.reset();
    expect(poll.question.length).toBe(0);
    expect(poll.answers.size).toBe(0);
})