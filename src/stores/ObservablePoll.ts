import { observable, computed, action } from 'mobx';

const empty_string: string = '';
const minAnswers: number = 2;
const maxAnswers: number = 10;
const reportSuffix: string = ' possible answers';

export class ObservablePoll {
    @observable question: string = 'What is the value of Pi?';
    @observable answers: Map<string, {text: string, counter: number}> = observable.map([
        ['3.14', { text: '3.14', counter: 5 }],
        ['3.1416', { text: '3.1416', counter: 2 }],
        ['3.1415964', { text: '3.1415964', counter: 1 }]
    ]);

    @computed get report(): string {
        return `${this.answers.size}/${maxAnswers}${reportSuffix}`;
    }

    @computed get voted(): number {
        return this.data.reduce((acc, value) => acc + value.counter, 0);
    }

    @computed get data(): Array<{text: string, counter: number}> {
        return [...this.answers.values()];
    }

    @computed get disabled(): boolean {
        return this.question.length === 80;
    }

    @action
    public addQuestion = (question: string) => {
        this.question = question;
    }
    
    @action
    public addAnswer = (answer: string) => {
        if (this.question.length && this.answers.size < maxAnswers && !this.answers.has(answer)) {
            this.answers.set(answer, {
                text: answer,
                counter: 0
            });
        }
    }

    @action
    public removeAnswer = (answer: string) => {
        if (this.answers.size > minAnswers) {
            this.answers.delete(answer);
        }
    }

    @action
    public toVote = (answer: string) => {
        const target = this.answers.get(answer);
        if (target) this.answers.set(answer, {text: answer, counter: target.counter + 1});
    }

    @action
    public reset = () => {
        this.answers = observable.map();
        this.question = empty_string;
    }
}