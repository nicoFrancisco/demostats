import "./poll.css";
import React from "react";
import { observer } from "mobx-react";
import { ObservablePoll } from "../../stores/ObservablePoll";
import { Answer } from "./components/answer/answer";
import { Question } from "./components/question/question";
import { EnterAnswer } from "./components/enter-answer/enter-answer";
import { Report } from "./components/report/report";

type Props = {
  store: ObservablePoll;
};

const PollComponent: React.FC<Props> = observer((props) => {
  const store = props.store;
  return (
    <div className="poll-wrapper">
      <Question
        value={store.question}
        disabled={store.disabled}
        onChange={store.addQuestion}
      />

      {store.data.map(
        (answer: { text: string; counter: number }, index: number) => (
          <Answer key={index} text={answer.text} onClick={store.removeAnswer} />
        )
      )}

      <EnterAnswer onClick={store.addAnswer} />

      <Report text={store.report} onClick={store.reset} />
    </div>
  );
});

export const Poll = PollComponent;
