export const maxLength: number = 80;
export const buttonAddText: string = 'Add';
export const addAnswerText: string = 'Type an answer';