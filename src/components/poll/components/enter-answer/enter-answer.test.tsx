import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { EnterAnswer } from './enter-answer';
import { addAnswerText } from './constants';

test('typing some text and calling add button', () => {
    const add = jest.fn();
    const { getByText, getByPlaceholderText } = render(<EnterAnswer onClick={add} />);
    const input = getByPlaceholderText(addAnswerText);
    const button = getByText(/Add/i);
    fireEvent.change(input, { target: { value: 'some answer' }});
    button.click();
    expect(button).toBeInTheDocument();
    expect(add).toBeCalled();
})