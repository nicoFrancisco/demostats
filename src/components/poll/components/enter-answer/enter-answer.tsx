import "./enter-answer.css";
import React, { useState } from "react";
import { addAnswerText, buttonAddText, maxLength } from "./constants";

type Props = {
  onClick: (answer: string) => void;
};

const EnterAnswerComponent: React.FC<Props> = (props) => {
  const [answer, setAnswer] = useState("");
  const handleAddAnswer = () => {
    props.onClick(answer);
    setAnswer("");
  };
  return (
    <div className="row enter-answer-wrapper">
      <div className="col-10">
        <input
          type="text"
          onChange={(e) => setAnswer(e.currentTarget.value)}
          maxLength={maxLength}
          placeholder={addAnswerText}
          className="input-answer"
          disabled={answer.length === 80}
          value={answer}
        />
      </div>
      <div className="col-2">
        <button
          disabled={answer === ""}
          className="enter-answer-button float-right"
          onClick={handleAddAnswer}
        >
          {buttonAddText}
        </button>
      </div>
    </div>
  );
};

export const EnterAnswer = EnterAnswerComponent;
