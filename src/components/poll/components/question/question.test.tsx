import React from 'react';
import { render } from '@testing-library/react';
import { Question } from './question';
import { ObservablePoll } from '../../../../stores/ObservablePoll';

const poll = new ObservablePoll();
const change = jest.fn();
const largeText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temporio';

test('enabled input with initial text less than 80 characters', () => {
    const { getByPlaceholderText } = render(
        <Question value={poll.question} disabled={poll.disabled} onChange={change} />
    );
    const input = getByPlaceholderText('Type a question');
    expect(input).toBeEnabled();
})


test('disabled input with text eq to 80 characters', () => {
    poll.addQuestion(largeText);
    const { getByPlaceholderText } = render(
        <Question value={poll.question} disabled={poll.disabled} onChange={change} />
    );
    const input = getByPlaceholderText('Type a question');
    expect(input).toBeDisabled();
})