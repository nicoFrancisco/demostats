import "./question.css";
import React from "react";

type Props = {
  value: string;
  disabled: boolean;
  onChange: (answer: string) => void;
};

const QuestionComponent: React.FC<Props> = (props) => {
  return (
    <div className="row">
      <div className="col-10">
        <input
          maxLength={80}
          className="question"
          type="text"
          placeholder="Type a question"
          value={props.value}
          disabled={props.disabled}
          onChange={(e) => props.onChange(e.currentTarget.value)}
        />
      </div>
    </div>
  );
};

export const Question = QuestionComponent;
