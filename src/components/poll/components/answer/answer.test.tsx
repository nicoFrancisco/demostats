import React from 'react';
import { render } from '@testing-library/react';
import { Answer } from './answer';
import { crossText } from './constants';

const remove = jest.fn();
const AnswerComponent = <Answer text="some answer" onClick={remove} />

test('answer text', () => {
    const { getByText } = render(AnswerComponent);
    const answerText = getByText(/some answer/i);
    expect(answerText).toBeInTheDocument();
})

test('onClick remove answer text', () => {
    render(AnswerComponent).getByText(crossText).click();
    expect(remove).toBeCalled();
})