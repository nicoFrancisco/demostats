import "./answer.css";
import React from "react";
import { crossText } from "./constants";

type Props = {
  text: string;
  onClick: (answer: string) => void;
};

const AnswerComponent: React.FC<Props> = (props) => {
  return (
    <div className="row answer-wrapper">
      <div className="col-10">
        <div className="answer">{props.text}</div>
      </div>
      <div className="col-2">
        <button
          className="answer-button-cross float-right"
          onClick={() => props.onClick(props.text)}
        >
          {crossText}
        </button>
      </div>
    </div>
  );
};

export const Answer = AnswerComponent;
