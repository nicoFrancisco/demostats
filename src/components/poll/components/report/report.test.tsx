import React from 'react';
import { render } from '@testing-library/react';
import { Report } from './report';
import { ObservablePoll } from '../../../../stores/ObservablePoll';

const poll = new ObservablePoll();
const click = jest.fn();
const report = <Report text={poll.report} onClick={click} />

test('report text', () => {
    const { getByText } = render(report);
    const reportText = getByText(/3\/10 possible answers/i);
    expect(reportText).toBeInTheDocument();
})

test('reset button click', () => {
    render(report).getByText(/Reset/i).click();
    expect(click).toBeCalled();
})