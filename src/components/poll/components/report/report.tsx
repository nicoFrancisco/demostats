import "./report.css";
import { resetText } from "./constants";
import React from "react";

type Props = {
  text: string;
  onClick: () => void;
};

const ReportComponent: React.FC<Props> = (props) => {
  return (
    <div className="row bottom">
      <div className="col-7 align-self-end">
        <span className="report-text">{props.text}</span>
      </div>
      <div className="col-4 align-self-end">
        <button className="button-reset" onClick={props.onClick}>
          {resetText}
        </button>
      </div>
    </div>
  );
};

export const Report = ReportComponent;
