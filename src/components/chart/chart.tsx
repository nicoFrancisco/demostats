import "./chart.css";
import { spec } from "./spec/rect";
import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { View, Spec, parse, changeset } from "vega";
import {
  renderer,
  defaultHeight,
  defaultWidth,
  defaultPadding,
  dataSource,
  totalVotes,
} from "./constants";
import { ObservablePoll } from "../../stores/ObservablePoll";
import { reaction } from "mobx";

type Props = {
  height?: number;
  width?: number;
  padding?: number;
  store: ObservablePoll;
};

const VegaChart: React.FC<Props> = observer((props) => {
  const chartRef: React.RefObject<HTMLDivElement> = React.createRef();
  let _spec: Spec = spec;
  let view!: View;

  /*componentDidMount() {
    reaction(
      () => props.store.data,
      (value) => {
        updateData(value);
        renderView();
      }
    );
    init();
  }*/
  useEffect(() => {
    reaction(
      () => props.store.data,
      (value) => {
        updateData(value);
        renderView();
      }
    );
    init();
  }, [props.store.data]);

  /*componentWillUnmount() {
    view.finalize();
  }*/
  useEffect(() => {
    //clear redux store when unmounting Detail
    return () => {
      view.finalize();
    };
  }, [view]);

  const init = (data?: any) => {
    applyProperties();
    initView();
    updateData(data);
    renderView();
  };

  const applyProperties = () => {
    _spec.height = props.height || defaultHeight;
    _spec.width = props.width || defaultWidth;
    _spec.padding = props.padding || defaultPadding;
  };

  const initView = () => {
    view = new View(parse(_spec))
      .initialize(chartRef.current!)
      .renderer(renderer)
      .hover();
  };

  const renderView = () => {
    view.run();
  };

  const updateData = (data: any = props.store.data) => {
    const dataSet = changeset()
      .remove(() => true)
      .insert(data);
    view.change(dataSource, dataSet);
  };

  /*const getSignals = () => {
    return view.getState().signals;
  };

  const getData = () => {
    return view.getState().data;
  };*/

  const poll = props.store;
  return (
    <div className="chart-wrapper">
      <div className="row justify-content-center">
        <div className="col-12">
          <div className="chart-question">{poll.question}</div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <div ref={chartRef}></div>
        </div>
      </div>
      <div className="row bottom">
        <div className="col-12">
          <span className="chart-total-votes-text">
            {totalVotes}
            {poll.voted}
          </span>
        </div>
      </div>
    </div>
  );
});

export const VgChart = VegaChart;
