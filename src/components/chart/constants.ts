import { Renderers } from "vega";

export const renderer: Renderers = 'canvas';
export const defaultHeight: number = 300;
export const defaultWidth: number = 200;
export const defaultPadding: number = 20;
export const dataSource: string = 'dataSource';
export const title: string = 'What is the value of Pi?';
export const totalVotes: string = 'Total votes: ';

