import { Spec } from 'vega';

export const spec: Spec = {
    $schema: "https://vega.github.io/schema/vega/v5.json",
    description: "A basic bar chart example, with value labels shown upon mouse hover.",
    autosize: "fit-x",
    width: 200,
    height: 300,
    padding: 0,
    data: [
        {
            name: 'dataSource',
            values: []
        }
    ],
    signals: [
        {
            name: "tooltip",
            value: {},
            on: [
                { events: "rect:mouseover", update: "datum" },
                { events: "rect:mouseout", update: "{}" }
            ]
        }
    ],
    scales: [
        {
            name: "xscale",
            type: "band",
            domain: { data: "dataSource", field: "text", sort: true },
            range: "width",
            padding: 0.05
        },
        {
            name: "yscale",
            type: "linear",
            domain: { data: "dataSource", field: "counter" },
            range: "height"
        }
    ],
    axes: [
        { orient: "bottom", scale: "xscale", labelLimit: 50 },
        { orient: "left", scale: "yscale", tickMinStep: 1 }
    ],
    marks: [
        {
            type: "rect",
            clip : true,
            from: { data: "dataSource" },
            encode: {
                update: {
                    fill: { value: "steelblue" },
                    x: { scale: "xscale", field: "text" },
                    width: { scale: "xscale", band: 1 },
                    y: { scale: "yscale", field: "counter" },
                    y2: { scale: "yscale", value: 0 }
                },
                hover: {
                    fill: { value: "red" }
                }
            }
        },
        {
            type: "text",
            encode: {
                enter: {
                    align: { value: "center" },
                    baseline: { value: "bottom" },
                    fill: { value: "#333" }
                },
                update: {
                    x: { scale: "xscale", signal: "tooltip.text", band: 0.5 },
                    y: { scale: "yscale", signal: "tooltip.counter", offset: -2 },
                    text: { signal: "tooltip.counter" },
                    fillOpacity: [
                        { test: "datum === tooltip", value: 0 },
                        { value: 1 }
                    ]
                }
            }
        }
    ]
}