import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Vote } from './vote';
import { ObservablePoll } from '../../stores/ObservablePoll';

const poll = new ObservablePoll();

test('select an answer and vote', () => {
    const toVote = jest.fn();
    const { getByText, getByPlaceholderText } = render(<Vote store={poll} onClick={toVote} />);
    const radio = getByPlaceholderText('3.14');
    const button = getByText(/Vote/i);
    fireEvent.click(radio);
    button.click();
    expect(radio).toBeChecked();
    expect(toVote).toBeCalled();
    expect(button).toBeInTheDocument();
})