import "./vote.css";
import React, { useState } from "react";
import { ObservablePoll } from "../../stores/ObservablePoll";
import { observer } from "mobx-react";
import { voteText } from "./constants";

type Props = {
  store: ObservablePoll;
  onClick: (text: string) => void;
};

const VoteComponent: React.FC<Props> = observer((props) => {
  const store = props.store;
  const [radioSelected, setRadioSelected] = useState("");

  const handleClickVote = () => {
    props.onClick(radioSelected);
  };

  return (
    <div className="vote-wrapper">
      <div className="row">
        <div className="col-12">
          <div className="vote-question">{store.question}</div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <ul className="vote-radio-list">
            {store.data.map(
              (answer: { text: string; counter: number }, index: number) => (
                <li key={index}>
                  <input
                    key={index}
                    type="radio"
                    name="vote"
                    checked={radioSelected === answer.text}
                    value={answer.text}
                    placeholder={answer.text}
                    onChange={() => setRadioSelected(answer.text)}
                  />
                  <span>{answer.text}</span>
                </li>
              )
            )}
          </ul>
        </div>
      </div>
      <div className="row bottom">
        <div className="col-7"></div>
        <div className="col-4">
          <button
            disabled={radioSelected === ""}
            className="vote-button"
            onClick={handleClickVote}
          >
            {voteText}
          </button>
        </div>
      </div>
    </div>
  );
});

export const Vote = VoteComponent;
