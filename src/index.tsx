import "./style/index.css";
import React from "react";
import ReactDOM from "react-dom";
import { App } from "./app/App";
import { ObservablePoll } from "./stores/ObservablePoll";
import { configure } from "mobx";

configure({ enforceActions: "observed" });

ReactDOM.render(
  <React.StrictMode>
    <App title="Demo Vega with Hooks" store={new ObservablePoll()} />
  </React.StrictMode>,
  document.getElementById("root")
);
